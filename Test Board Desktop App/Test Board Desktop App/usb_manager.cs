﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using System.Threading;
using System.Windows.Threading;
using System.Collections;

namespace Test_Board_Desktop_App
{
    public class usb_manager
    {
        /// <summary>
        /// Callback for completion of the connection, successfull or not.
        /// </summary>
        public event EventHandler ConnectionCompleted;
        public event EventHandler<string> ErrorHandler;

        /// <summary>
        /// Number of retries to establish comms.
        /// </summary>
        public const int RETRIES = 3;

        /// <summary>
        /// The port being used for USB comms
        /// </summary>
        private SerialPort _serialPort = new SerialPort();

        /// <summary>
        /// Used to clean up running threads.
        /// </summary>
        private volatile bool _running = false;
        /// <summary>
        ///  The thread responsible for reading information from a device.
        /// </summary>
        private Thread tRead = null;
        /// <summary>
        ///  The thread responsible for writing information to a device.
        /// </summary>
        private Thread tWrite = null;
        /// <summary>
        /// The thread responsible for connection to a device
        /// </summary>
        private Thread tConnect = null;
        
        /// <summary>
        /// Indicates if there is a test board currently connected.
        /// </summary>
        public bool Connected { get; private set; }

        /// <summary>
        /// Indicates if the comm port is open.
        /// </summary>
        public bool IsPortOpen
        {
            get
            {
                return _serialPort.IsOpen;
            }
        }

        /// <summary>
        /// Current number of tries to establish comms.
        /// </summary>
        public int TryNumber { get; private set; } = 0;

        public Queue<PFN.PacketFrame> rxFrames = new Queue<PFN.PacketFrame>();
        public Queue<PFN.PacketFrame> txFrames = new Queue<PFN.PacketFrame>();
        public usb_manager()
        {
            //Serial port values are the same as the PFN parameters.
            _serialPort.BaudRate = 115200;
            _serialPort.Parity = Parity.Odd;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
        }
        /// <summary>
        /// Starts the process of connecting the test board
        /// </summary>
        /// <param name="portName">Hardware port name of the usb port of the host computer</param>
        public void BeginConnect(string portName)
        {
            try
            {
                if (_serialPort.IsOpen)
                {
                    _serialPort.DiscardInBuffer();
                    _serialPort.Close();
                }
                if (tConnect == null)
                {
                    _serialPort.PortName = portName;
                    tConnect = new Thread(ConnectThread);
                    tConnect.IsBackground = true;
                    tConnect.Start();
                }
            }
            catch (System.IO.IOException e)
            {
                ErrorHandler(this,e.Message);
            }
        }

        /// <summary>
        /// Thread responsible for connection to the board.
        /// </summary>
        private void ConnectThread()
        {
            TryNumber = 0;
            Connected = false;

            while (!_serialPort.IsOpen && TryNumber < RETRIES && !Connected)
            {
                ++TryNumber;
                try
                {
                    _serialPort.Open();

                    //clear out anything in the buffer
                    _serialPort.DiscardInBuffer();                   
                   
                }
                catch(System.IO.IOException e)
                {                 
                   ErrorHandler(this, e.Message);
                }
                
                Thread.Sleep(0);
            }

            if (Connected)
            {
                tRead = new Thread(ReadThread);
                tRead.IsBackground = true;
                tRead.Start();

                tWrite = new Thread(WriteThread);
                tWrite.IsBackground = true;
                tWrite.Start();
            }
            else if (_serialPort.IsOpen)
            {
                _serialPort.Close();
            }

            ConnectionCompleted(this, new EventArgs());

            tConnect = null;
        }

        /// <summary>
        /// Disconnects and closes the USB port.
        /// </summary>
        public void Disconnect()
        {
            _running = false;

            //Wait for the threads to shut down.
            while (tRead.IsAlive && tWrite.IsAlive && tConnect.IsAlive) ;

            try
            {
                _serialPort.Close();
            }
            catch { }

            ConnectionCompleted(this, new EventArgs());
        }

        /// <summary>
        /// Thread for reading the data in the usb input buffer
        /// </summary>
        public void ReadThread()
        {
            PFN.PacketFrame packetFrame;
            _running = true;
            while (_running)
            {
                try
                {
                    if (_serialPort.BytesToRead > 0)
                    {
                        //Check for the beginning of a frame
                        if (_serialPort.ReadByte() == PFN.PREAMBLE)
                        {
                            packetFrame = new PFN.PacketFrame();
                            packetFrame.preamble = PFN.PREAMBLE;

                            packetFrame.header.slaveAddress = (byte)_serialPort.ReadByte();
                            packetFrame.header.pfnCompatibilityVersion = (UInt32)(_serialPort.ReadByte() << 24);
                            packetFrame.header.pfnCompatibilityVersion |= (UInt32)(_serialPort.ReadByte() << 16);
                            packetFrame.header.pfnCompatibilityVersion |= (UInt32)(_serialPort.ReadByte() << 8);
                            packetFrame.header.pfnCompatibilityVersion |= (UInt32)(_serialPort.ReadByte());
                            packetFrame.header.hwType = (byte)_serialPort.ReadByte();
                            packetFrame.header.variant = (byte)_serialPort.ReadByte();

                            packetFrame.subframe.frameType = (PFN.FrameType)_serialPort.ReadByte();
                            packetFrame.subframe.reserved = (byte)_serialPort.ReadByte();
                            packetFrame.subframe.sequenceNumber = (ushort)(_serialPort.ReadByte() << 8);
                            packetFrame.subframe.sequenceNumber |= (ushort)_serialPort.ReadByte();
                            packetFrame.subframe.command = (byte)_serialPort.ReadByte();
                            packetFrame.subframe.ackNack = (byte)_serialPort.ReadByte();
                            packetFrame.subframe.nackCode = (PFN.NackCode)_serialPort.ReadByte();
                            packetFrame.subframe.payloadLength = (byte)_serialPort.ReadByte();

                            _serialPort.Read(packetFrame.subframe.data, 0, packetFrame.subframe.payloadLength);

                            packetFrame.crc = (UInt16)(_serialPort.ReadByte() << 8);
                            packetFrame.crc |= (UInt16)_serialPort.ReadByte();

                            if (PFN.ValidateFrame(packetFrame))
                            {
                                lock (rxFrames)
                                    rxFrames.Enqueue(packetFrame);
                            }

                            PFN.SequenceNumber = (UInt16)(packetFrame.subframe.sequenceNumber + 1);
                        }
                    }
                }
                catch (System.IO.IOException e)
                {
                    try
                    {
                        Disconnect();
                    }
                    catch { }

                    ErrorHandler(this, e.Message);
                }
                
                Thread.Sleep(0);
            }

            tRead = null;
        }

        /// <summary>
        /// A thread to manage the writing to the USB device
        /// </summary>
        private void WriteThread()
        {
            PFN.PacketFrame frame = new PFN.PacketFrame();

            _running = true;
            while (_running)
            {
                if(txFrames.Count > 0 && _serialPort.IsOpen)
                {
                    lock (txFrames)
                        frame = txFrames.Dequeue();

                    byte[] data = PFN.PackWithCrc(frame);
                    _serialPort.Write(data,0, data.Length);
                }
                Thread.Sleep(0);
            }

            tWrite = null;
        }

        /// <summary>
        /// Enques a frame to be written to a connected device.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="command"></param>
        public void Write(byte[] data, byte command)
        {
            PFN.PacketFrame frame = new PFN.PacketFrame();

            frame.preamble = PFN.PREAMBLE;
            frame.subframe.frameType = 0;
            frame.subframe.sequenceNumber = (UInt16)(PFN.SequenceNumber + 1);
            frame.subframe.command = command;
            frame.subframe.ackNack = 0;
            frame.subframe.nackCode = PFN.NackCode.NO_NACK;
            frame.subframe.payloadLength = (byte)data.Length;
            frame.subframe.data = data;

            frame.crc = PFN.PfnCrc.CalculateCrC16(PFN.PackWithoutCrc(frame), PFN.CRC_INIT_VAL);

            lock (txFrames)
                txFrames.Enqueue(frame);
        }
    }
}