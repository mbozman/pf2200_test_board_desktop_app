﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Board_Desktop_App
{
    public class Crc
    {
        private UInt32 _polynomial;

        static readonly ushort[] table = new ushort[256];

        public Crc(UInt32 polynomial)
        {
            _polynomial = polynomial;
            ushort value;
            ushort temp;
            for (ushort i = 0; i < table.Length; ++i)
            {
                value = 0;
                temp = i;
                for (byte j = 0; j < 8; ++j)
                {
                    if (((value ^ temp) & 0x0001) != 0)
                    {
                        value = (ushort)((value >> 1) ^ polynomial);
                    }
                    else
                    {
                        value >>= 1;
                    }
                    temp >>= 1;
                }
                table[i] = value;
            }
        }
        public UInt16 CalculateCrC16(byte[] data, ushort initVal = 0)
        {
            ushort[] table = new ushort[256];
            ushort temp, a;
            ushort crc = initVal;
            for (int i = 0; i < table.Length; ++i)
            {
                temp = 0;
                a = (ushort)(i << 8);
                for (int j = 0; j < 8; ++j)
                {
                    if (((temp ^ a) & 0x8000) != 0)
                        temp = (ushort)((temp << 1) ^ _polynomial);
                    else
                        temp <<= 1;
                    a <<= 1;
                }
                table[i] = temp;
            }
            for (int i = 0; i < data.Length; ++i)
            {
                crc = (ushort)((crc << 8) ^ table[((crc >> 8) ^ (0xff & data[i]))]);
            }
            return crc;
        }
    }
}
