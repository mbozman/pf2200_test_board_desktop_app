﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Test_Board_Desktop_App
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private usb_manager UsbManager = new usb_manager(); //Manages the USB communications
        public MainWindow()
        {
            InitializeComponent();

            //Initialize UI components.
            _comboUsbPorts.ItemsSource = SerialPort.GetPortNames();
            _comboUsbPorts.SelectedIndex = 0;

            __statusBarComms.Content = "Disconnected";
            //Assign events and delegates
            UsbManager.ConnectionCompleted += UsbManager_ConnectionCompleted;

            UsbManager.ErrorHandler += ErrorHandler;
        }

        /// <summary>
        /// Callback from connection thread indicating completion. Re-enables the applicable inputs.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UsbManager_ConnectionCompleted(object sender, EventArgs e)
        {
            Dispatcher.Invoke(new Action(() =>
            {
                if (UsbManager.Connected)
                {
                    _btnConnect.Content = "Disconnect";
                    __statusBarComms.Content = "Connected";
                }
                else
                {
                    _btnConnect.Content = "Connect";
                    __statusBarComms.Content = "Disconnected";
                }
                _comboUsbPorts.IsEnabled = true;
                _btnConnect.IsEnabled = true;
            }));
        }

        /// <summary>
        /// Relays an error message to the status bar.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="message"></param>
        private void ErrorHandler(object sender, string message)
        {
            Dispatcher.Invoke(new Action(() => _statusBar.Items[3] = message));
        }

        /// <summary>
        /// Initiates the connection with the test board
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _btnConnect_Click(object sender, RoutedEventArgs e)
        {
            if (_btnConnect.Content.ToString() == "Connect")
            {
                _btnConnect.IsEnabled = false;
                _btnConnect.Content = "Connecting...";
                __statusBarComms.Content = "Connecting";
                _comboUsbPorts.IsEnabled = false;

                string port = _comboUsbPorts.SelectedValue.ToString();
                UsbManager.BeginConnect(port);
            }
            else
            {
                _comboUsbPorts.IsEnabled = false;
                _btnConnect.IsEnabled = false;
                _btnConnect.Content = "Disconnecting...";
                __statusBarComms.Content = "Disconnecting";
                UsbManager.Disconnect();
            }
        }
    }
}
