﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Board_Desktop_App
{
    public static class PFN
    {
        public const UInt16 PREAMBLE = 0x556E;
        private const UInt32 CRC_POLYNOMIAL = 0x755B;
        public const ushort CRC_INIT_VAL = 0;
        private const UInt32 PFN_COMPATIBILITY_VERSION = 1;

        public static Crc PfnCrc { get; private set; }

        public static UInt16 SequenceNumber { get; set; } = 0;
        public struct PacketFrame
        {
            public UInt16 preamble;
            public Header header;
            public Subframe subframe;
            public UInt16 crc;
        }

        public struct Header
        {
            public byte slaveAddress;
            public UInt32 pfnCompatibilityVersion;
            public byte hwType;
            public byte variant;
        }
        public struct Subframe
        {
            public FrameType frameType;
            public byte reserved;
            public UInt16 sequenceNumber;
            public byte command;
            public byte ackNack;
            public NackCode nackCode;
            public byte payloadLength;
            public byte[] data;
        }

        /// <summary>
        /// The Frame ID is used to distinguish different types of packets. 
        /// The Frame ID field consists of one byt
        /// </summary>
        public enum FrameType : byte
        {
            AUTHENTICATION_REQUEST = 0x1A,
            AUTHENTICATION_RESPONSE = 0x1B,
            INTERFACE_REQUEST = 0x2A,
            INTERFACE_RESPONSE = 0x2B,
            REGISTER_ACCESS_REQUEST = 0x3A,
            REGISTER_ACCESS_RESPONSE = 0x3B,
            EVENT_LOG_REQUEST = 0x4A,
            EVENT_LOG_RESPONSE = 0x4B,
            DISCOVERY_REQUEST = 0x5A,
            DISCOVERY_RESPONSE = 0x5B,
            FIRMWARE_UPDATE_REUEST = 0x6A,
            FIRMWARE_UPDATE_RESPONSE = 0x6B,
            REMOTE_CONTROL_REQUEST = 0x7A,
            REMOTE_CONTROL_RESPONSE = 0x7B,
            DESCRIPTOR_ACCESS_REQUEST = 0x8A,
            DESCRIPTOR_ACCESS_RESPONSE = 0x8B
        }

        public enum NackCode : byte
        {
            NO_NACK = 0,
            NOT_READY,
            BAD_ADDRESS,
            AUTHENTICATION_FAILED,
            INVALID_REMOTE_REQUEST,
            READ_ONLY,
            CANT_WRITE_WHILE_RUNNING,
            UNSUSED,
            OUT_OF_BOUNDS,
            UNUSED,
            INVALID_REQUEST,
            IDENTIFIER_MISMATCH
        }

        /// <summary>
        /// Static constructor for the PFN object
        /// </summary>
        static PFN()
        {
            PfnCrc = new Crc(CRC_POLYNOMIAL);
        }
        /// <summary>
        /// Used to verify that the passed in frame is a valid frame.
        /// </summary>
        /// <param name="frame"></param>
        /// <returns></returns>
        public static bool ValidateFrame(PacketFrame frame)
        {
            if (frame.preamble != PREAMBLE)
                return false;

            if (frame.header.pfnCompatibilityVersion != PFN_COMPATIBILITY_VERSION)
                return false;

            if(frame.header.)

            if (frame.subframe.sequenceNumber <= SequenceNumber)
                return false;

            if (frame.crc != PfnCrc.CalculateCrC16(PackWithoutCrc(frame), CRC_INIT_VAL))
                return false;

            return true;
        }

        public static byte[] PackWithoutCrc(PacketFrame frame)
        {
            List<byte> packArr = new List<byte>();


            packArr.AddRange(BitConverter.GetBytes(frame.preamble));

            packArr.Add(frame.header.slaveAddress);
            packArr.AddRange(BitConverter.GetBytes(frame.header.pfnCompatibilityVersion));
            packArr.Add(frame.header.hwType);
            packArr.Add(frame.header.variant);

            packArr.Add((byte)frame.subframe.frameType);
            packArr.Add((byte)frame.subframe.reserved);
            packArr.AddRange(BitConverter.GetBytes(frame.subframe.sequenceNumber));
            packArr.Add((byte)frame.subframe.command);
            packArr.Add((byte)frame.subframe.ackNack);
            packArr.Add((byte)frame.subframe.nackCode);
            packArr.Add((byte)frame.subframe.payloadLength);
            packArr.AddRange(frame.subframe.data);

            return packArr.ToArray();
        }

        public static byte[] PackWithCrc(PacketFrame frame)
        {
            List<byte> packArr = new List<byte>(PackWithoutCrc(frame));
            packArr.AddRange(BitConverter.GetBytes(frame.crc));
            return packArr.ToArray();
        }
    }
}
